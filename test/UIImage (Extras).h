//
//  UIImage (Extras).h
//  GPSDataGathering
//
//  Created by Jermin Bazazian on 1/11/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIImage (Extras)
- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize;
@end
