//
//  StartPageController.m
//  test
//
//  Created by Tiseno Mac 2 on 12/30/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "StartPageController.h"

@implementation StartPageController
//@synthesize imgCamView;
//@synthesize image1;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(IBAction)startTapped:(id)button
{
    
    
    
    UIImagePickerController *timagePickerController = [[UIImagePickerController alloc] init];
     timagePickerController.delegate = self;
     timagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
     ////UIImagePickerControllerSourceTypeSavedPhotosAlbum;
     
     timagePickerController.allowsEditing=NO;
     [self presentModalViewController:timagePickerController animated:YES];
    
     
    
    /*--new enter 30/12 2011--*/
    
    /*LoginPageController *GLoginPageController=[[LoginPageController alloc] initWithNibName:@"LoginPageController"  bundle:nil];

    [self.view addSubview:GLoginPageController.view];*/
    
  
    
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo 
{
    [picker dismissModalViewControllerAnimated:YES];
        
    ScrollViewController *scorllViewController=[[ScrollViewController alloc] initWithNibName:@"ScrollViewController"  bundle:nil];
    scorllViewController.image1=image;
    
    //scorllViewController
    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:scorllViewController];
    
    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    for(UIView *tview in self.view.subviews)
    {
        [tview removeFromSuperview];
    }
    [self.view addSubview:tnavController.view];
    
    }

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[self dismissModalViewControllerAnimated:YES];
   
}

- (void)dealloc 
{

    //[image1 release];
    
    //[imgCamView release];
    [super dealloc];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
        
}

@end
