//
//  ScrollViewController.h
//  slideviewtest
//
//  Created by Tiseno Mac 2 on 12/5/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadGalleryController.h"
#import "testClasssAppDelegate.h"

@interface ScrollViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate, UIScrollViewDelegate>{
    UIImagePickerController *imagePickerController;
    UIScrollView *scrollView;
    UIScrollView *scrollCamView;
    UIImageView *CamPictureImgView;
    UILabel *pageNumberLabel;
    UIImage *image1;
    UIButton *CamRetake;
    //IBOutlet UILabel *pageNumberLabel;
    int pageNumber;
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;
    UIPageControl *pageControl;
    
}


@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;

- (id)initWithPageNumber:(int)page;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollCamView;
@property (retain, nonatomic) IBOutlet UILabel *pageNumberLabel;
@property (nonatomic, retain) NSString *StageDesc;

@property (retain, nonatomic) IBOutlet UIImageView *CamPictureImgView;

@property (nonatomic, retain) NSMutableArray *viewControllers;
@property (nonatomic,retain) UIImage *image1;

-(IBAction)CamRetakeTapped:(id)button;
-(IBAction)CamPicStageConfirmTapped:(id)button;
-(void)switchToUploadGalleryController;

@end
