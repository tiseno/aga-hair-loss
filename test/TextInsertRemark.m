//
//  TextInsertRemark.m
//  test
//
//  Created by Tiseno Mac 2 on 12/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "TextInsertRemark.h"




@implementation TextInsertRemark
@synthesize image1,_gtext,StringRemark,_textPatientName,_TmpStringRemark;
@synthesize RemarkTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    [[RemarkTextField layer] setBorderWidth:2.3];
	[[RemarkTextField layer] setCornerRadius:15];
    //[self.RemarkTextField becomeFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)dealloc
{
    [RemarkTextField release];
    //[leftBarButtonItem release];
    [StringRemark release];
    [image1 release];
    [_gtext release];
    [_textPatientName release];
    [_TmpStringRemark release];
    [super dealloc];
}





-(IBAction)BtnConfirmRemark:(id)sender
{
    
    self.StringRemark=self.RemarkTextField.text;
    //NSString *nameString = self.PatientName;
    //if ([nameString length]==0){
    //nameString=@"World";
    
    //}
    //NSString *greeting =[[NSString alloc]initWithFormat:@"Hello, %@!",nameString];
    //self.lable.text=greeting;
    
    
    UploadGalleryController *GUploadGalleryController=[[UploadGalleryController alloc] initWithNibName:@"UploadGalleryController"  bundle:nil];
    GUploadGalleryController.image1= self.image1;
    GUploadGalleryController._textPatientName=self._textPatientName;
    
    GUploadGalleryController._text=self._gtext;
    
    
    
    self.StringRemark=self.RemarkTextField.text;

    GUploadGalleryController._textRemark=self.StringRemark;
    
    
    /*UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:GUploadGalleryController];
    
    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:tnavController.view];*/
    
	
    //[self.view addSubview:GUploadGalleryController.view];
    [self.RemarkTextField resignFirstResponder];
    //[self.navigationController pushViewController:GUploadGalleryController animated:YES];
    [self presentModalViewController:GUploadGalleryController animated:YES];

    
}

-(IBAction)BtnCancelRemark:(id)sender
{
    UploadGalleryController *GUploadGalleryController=[[UploadGalleryController alloc] initWithNibName:@"UploadGalleryController"  bundle:nil];
    GUploadGalleryController.image1= self.image1;
    GUploadGalleryController._textPatientName=self._textPatientName;
    
    GUploadGalleryController._text=self._gtext;
    
        
    //self.StringRemark=self.RemarkTextField.text;
    
    GUploadGalleryController._textRemark=self._TmpStringRemark;
    
    
    /*UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:GUploadGalleryController];
    
    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:tnavController.view];*/
    
	
    [self.view addSubview:GUploadGalleryController.view];
    [self.RemarkTextField resignFirstResponder];

}


-(void)viewDidDisappear:(BOOL)animated 
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

/*-(void)keyboardWillShow:(NSNotification *)aNotification 
{
	CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.size.height -= keyboardRect.size.height;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

-(void)keyboardWillHide:(NSNotification *)aNotification
{
   	CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.size.height += keyboardRect.size.height;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}*/

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	// provide my own Save button to dismiss the keyboard
	UIBarButtonItem* saveItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
																			  target:self action:@selector(BtnConfirmRemark:)];
	self.navigationItem.rightBarButtonItem = saveItem;
	[saveItem release];
}


@end
