//
//  testClasssViewController.h
//  test
//
//  Created by Tiseno Mac 2 on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "CameraViewController.h"
#import "ScrollViewController.h" 
#import "UploadGalleryController.h"
#import "StartPageController.h"

@interface testClasssViewController : UIViewController<UINavigationControllerDelegate, UIScrollViewDelegate,UITextFieldDelegate>{
    UIImagePickerController *imagePickerController;
    
    UIImageView *imgCamView;
    UIImage *image1;

    
    UITextField *UserNameText;
    UITextField *PasswordText;  
    NSString *_gUserNametext;
    NSString *_gPasswordtext;

}
//@property (retain, nonatomic) IBOutlet UIPickerView *categoryPickerView;

@property (retain, nonatomic) IBOutlet NSObject *MainViewObjectDelegate;

@property (retain, nonatomic) IBOutlet UIViewController *MainViewController;

@property (retain, nonatomic) IBOutlet UIImageView *imgCamView;

//@property (nonatomic) NSInteger dm_pickerIndex;

//@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;

-(IBAction)startTapped:(id)button;

@property (nonatomic,retain) UIImage *image1;
@property (nonatomic, retain) NSString *uploadedImage1Path;

@property (retain, nonatomic) IBOutlet UIImageView *LoginBackgroundView;
@property (retain, nonatomic) IBOutlet UIImageView *TxtUserLogin;
@property (retain, nonatomic) IBOutlet UILabel *lblUsername;
@property (retain, nonatomic) IBOutlet UILabel *lblPassword;


@property (retain, nonatomic) IBOutlet UITextField *UserNameText;
@property (retain, nonatomic) IBOutlet UITextField *PasswordText;
@property (nonatomic,retain) NSString *_gUserNametext;
@property (nonatomic,retain) NSString *_gPasswordtext;

//-(IBAction)save:(id)sender;

//-(void)saveInAFile;
//-(NSString*)uploadImage:(UIImageView*)imageView;


@end
