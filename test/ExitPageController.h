//
//  ExitPageController.h
//  test
//
//  Created by Tiseno Mac 2 on 12/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "testClasssViewController.h"

@interface ExitPageController : UIViewController<UIApplicationDelegate>
{
    int counterA;
    bool startA;
    NSString *_CountDownTime;
    NSTimer *timerA;
    IBOutlet UILabel *secondsA;

}

@property (nonatomic,retain) NSString *_CountDownTime;
//- (IBAction)start:(id)sender;
- (void) updateTimer;


@end
