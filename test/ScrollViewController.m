//
//  ScrollViewController.m
//  slideviewtest
//
//  Created by Tiseno Mac 2 on 12/5/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ScrollViewController.h"
 
static NSUInteger kNumberOfPages = 13;
//static NSArray *__pageControlColorList = nil;

@implementation ScrollViewController
@synthesize scrollView;
@synthesize scrollCamView;
@synthesize pageNumberLabel;
@synthesize CamPictureImgView;
//@synthesize pageNumberLabel;
@synthesize image1;
@synthesize viewControllers;
@synthesize pageControl;
@synthesize StageDesc;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
/*+ (UIColor *)pageControlColorWithIndex:(NSUInteger)index {
    if (__pageControlColorList == nil) {
        __pageControlColorList = [[NSArray alloc] initWithObjects:[UIColor redColor], [UIColor greenColor], [UIColor magentaColor],
                                  [UIColor blueColor], [UIColor orangeColor], [UIColor brownColor], [UIColor grayColor], nil];
    }
    // Mod the index by the list length to ensure access remains in bounds.
    return [__pageControlColorList objectAtIndex:index % [__pageControlColorList count]];
}*/
- (id)initWithPageNumber:(int)page {
    if (self = [super initWithNibName:@"ScrollViewController" bundle:nil]) {
        pageNumber = page;
    }
    return self;
}
#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.CamPictureImgView.image=self.image1;
    
    /*UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
     self.navigationItem.backBarButtonItem = backButton;
     [[self navigationItem] setBackBarButtonItem:backButton];*/
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++) {
        [controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    [controllers release];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    scrollView.contentSize=CGSizeMake(320*12, 190);
    
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = NO;
    scrollView.delegate = self;
	
    pageControl.numberOfPages = kNumberOfPages;
    pageControl.currentPage = 0;
    
    
    
    UIImageView *imgCamView1=[[UIImageView alloc] initWithImage:self.image1];
    imgCamView1.frame=CGRectMake(0, -20, 320, 400);
    [scrollCamView addSubview:imgCamView1];
    scrollCamView.contentSize=CGSizeMake(320, 300);
    
    UIImageView *imgView1=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_1.png"]];
    imgView1.frame=CGRectMake(0, 0, 320, 190);
    [scrollView addSubview:imgView1];
    
    //[self imageStage];
    self.pageNumberLabel.text=@"Stage 1";
        
    /*UIImageView *imgView2=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_3.png"]];
    imgView2.frame=CGRectMake(300, 0, 300, 160);
    [scrollView addSubview:imgView2];*/
    
    
	
    // pages are created on demand
    // load the visible page
    // load the page on either side to avoid flashes when the user starts scrolling
    
    
    //[self initWithPageNumber:0];
    //[self initWithPageNumber:1];

    //[self loadScrollViewWithPage:0];
    //[self loadScrollViewWithPage:1];
    
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
    //self.navigationController.navigationBarHidden=YES;
    
    //pageNumberLabel.text = [NSString stringWithFormat:@"Page %d", pageNumber + 1];
    //self.view.backgroundColor = [ScrollViewController pageControlColorWithIndex:pageNumber];
}


- (void)loadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
	
    
    // replace the placeholder if necessary
    ScrollViewController *controller = [viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null]) {
        controller = [[ScrollViewController alloc] initWithPageNumber:page];
        [viewControllers replaceObjectAtIndex:page withObject:controller];
        [controller release];
    }
    
    if (page == 1)
    {
        self.pageNumberLabel.text=@"Stage 1";
        self.StageDesc=@"Full head of hair without any hair loss.";
    }
    if (page == 2)
    {
        self.pageNumberLabel.text=@"Stage 2";
        self.StageDesc=@"Minor recession at the front of the hairline.";
    }
    if (page == 3)
    {
        self.pageNumberLabel.text=@"Stage 2(anterior)";
        self.StageDesc=@"Recession across entire frontal hairline.";
    }
    if (page == 4)
    {
        self.pageNumberLabel.text=@"Stage 3";
        self.StageDesc=@"Further loss at the front of the hairline, which is considered 'cosmetically significant'.";
    }
    if (page == 5)
    {
        self.pageNumberLabel.text=@"Stage 3(anterior)";
        self.StageDesc=@"Significant recession at templess along with receding hairline.";
    }
    if (page == 6)
    {
        self.pageNumberLabel.text=@"Stage 3(vertex)";
        self.StageDesc=@"Bald spot develops at the crown in addition to significant hair loss at the front of the hairline.";
    }
    if (page == 7)
    {
        self.pageNumberLabel.text=@"Stage 4";
        self.StageDesc=@"Progressively more loss along the front hairline and at the crown.";
    }
    if (page == 8)
    {
        self.pageNumberLabel.text=@"Stage 4(anterior)";
        self.StageDesc=@"Hair moves past midcrown.";
    }
    if (page == 9)
    {
        self.pageNumberLabel.text=@"Stage 5";
        self.StageDesc=@"Hair loss extends toward the vertex.";
    }
    if (page == 10)
    {
        self.pageNumberLabel.text=@"Stage 5(anterior)";
        self.StageDesc=@"Hair loss extends toward the vertex, back part of bald area is narrower than in stage 5.";
    }
    if (page == 11)
    {
        self.pageNumberLabel.text=@"Stage 6";
        self.StageDesc=@"Frontal and vertex balding areas merge into one and increase in size.";
    }
    if (page == 12)
    {
        self.pageNumberLabel.text=@"Stage 7";
        self.StageDesc=@"The last stage of male-pattern baldness, in which all hair is lost along the front hairline and crown.";
    }
        
    
    // add the controller's view to the scroll view
    if (nil == controller.view.superview) {
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        
        
        UIImageView *imgView1=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_1.png"]];
        imgView1.frame=CGRectMake(0, 0, 320, 190);
        [scrollView addSubview:imgView1];
        
        
        UIImageView *imgView2=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_2.png"]];
        imgView2.frame=CGRectMake(320, 0, 320, 190);
        [scrollView addSubview:imgView2];
        
        UIImageView *imgView3=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_2_anterior.png"]];
        imgView3.frame=CGRectMake(320*2, 0, 320, 190);
        [scrollView addSubview:imgView3];
        
        UIImageView *imgView4=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_3.png"]];
        imgView4.frame=CGRectMake(320*3, 0, 320, 190);
        [scrollView addSubview:imgView4];
        
        UIImageView *imgView5=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_3_anterior .png"]];
        imgView5.frame=CGRectMake(320*4, 0, 320, 190);
        [scrollView addSubview:imgView5];
        
        UIImageView *imgView6=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_3_vertex.png"]];
        imgView6.frame=CGRectMake(320*5, 0, 320, 190);
        [scrollView addSubview:imgView6];
        
        UIImageView *imgView7=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_4.png"]];
        imgView7.frame=CGRectMake(320*6, 0, 320, 190);
        [scrollView addSubview:imgView7];
        
        UIImageView *imgView8=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_4_anterior.png"]];
        imgView8.frame=CGRectMake(320*7, 0, 320, 190);
        [scrollView addSubview:imgView8];
        
        UIImageView *imgView9=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_5.png"]];
        imgView9.frame=CGRectMake(320*8, 0, 320, 190);
        [scrollView addSubview:imgView9];
        
        UIImageView *imgView10=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_5_anterior.png"]];
        imgView10.frame=CGRectMake(320*9, 0, 320, 190);
        [scrollView addSubview:imgView10];
        
        UIImageView *imgView11=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_6.png"]];
        imgView11.frame=CGRectMake(320*10, 0, 320, 190);
        [scrollView addSubview:imgView11];
        
        UIImageView *imgView12=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_7.png"]];
        imgView12.frame=CGRectMake(320*11, 0, 320, 190);
        [scrollView addSubview:imgView12];
        
        //[self imageStage];
        [scrollView addSubview:controller.view];
    }
}

- (void)imageStage
{
    UIImageView *imgView1=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_1.png"]];
    imgView1.frame=CGRectMake(0, 0, 320, 190);
    [scrollView addSubview:imgView1];
    
    
    UIImageView *imgView2=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_2.png"]];
    imgView2.frame=CGRectMake(320, 0, 320, 190);
    [scrollView addSubview:imgView2];
    
    UIImageView *imgView3=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_2_anterior.png"]];
    imgView3.frame=CGRectMake(320*2, 0, 320, 190);
    [scrollView addSubview:imgView3];
    
    UIImageView *imgView4=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_3.png"]];
    imgView4.frame=CGRectMake(320*3, 0, 320, 190);
    [scrollView addSubview:imgView4];
    
    UIImageView *imgView5=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_3_anterior .png"]];
    imgView5.frame=CGRectMake(320*4, 0, 320, 190);
    [scrollView addSubview:imgView5];
    
    UIImageView *imgView6=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_3_vertex.png"]];
    imgView6.frame=CGRectMake(320*5, 0, 320, 190);
    [scrollView addSubview:imgView6];
    
    UIImageView *imgView7=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_4.png"]];
    imgView7.frame=CGRectMake(320*6, 0, 320, 190);
    [scrollView addSubview:imgView7];
    
    UIImageView *imgView8=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_4_anterior.png"]];
    imgView8.frame=CGRectMake(320*7, 0, 320, 190);
    [scrollView addSubview:imgView8];
    
    UIImageView *imgView9=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_5.png"]];
    imgView9.frame=CGRectMake(320*8, 0, 320, 190);
    [scrollView addSubview:imgView9];
    
    UIImageView *imgView10=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_5_anterior.png"]];
    imgView10.frame=CGRectMake(320*9, 0, 320, 190);
    [scrollView addSubview:imgView10];
    
    UIImageView *imgView11=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_6.png"]];
    imgView11.frame=CGRectMake(320*10, 0, 320, 190);
    [scrollView addSubview:imgView11];
    
    UIImageView *imgView12=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_7.png"]];
    imgView12.frame=CGRectMake(320*11, 0, 320, 190);
    [scrollView addSubview:imgView12];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    
    
    
    
    if (pageControlUsed) {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 100) / pageWidth) + 1;
    pageControl.currentPage = page;
	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
	
    // A possible optimization would be to unload the views+controllers which are no longer visible
}

- (void)viewDidUnload
{
    [self setCamPictureImgView:nil];
    [self setScrollCamView:nil];
    [self setPageNumberLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)CamRetakeTapped:(id)button
{
    UIImagePickerController *timagePickerController = [[UIImagePickerController alloc] init];
    timagePickerController.delegate = self;
    timagePickerController.sourceType = 
    ////UIImagePickerControllerSourceTypeSavedPhotosAlbum;
	UIImagePickerControllerSourceTypeCamera;
	timagePickerController.allowsEditing=NO;
    [self presentModalViewController:timagePickerController animated:YES];
}

- (void)dealloc 
{
    [CamPictureImgView release];
    [scrollView release];
    [scrollCamView release];
    [pageNumberLabel release];
    [CamRetake release];
    [image1 release];
    [pageNumberLabel release];
    
    [super dealloc];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo 
{
    [picker dismissModalViewControllerAnimated:YES];
    
    //[picker.parentViewController dismissModalViewControllerAnimated:YES];
    self.image1=image;
    for(UIView* uiview in scrollView.subviews)
    {
        [uiview removeFromSuperview];
    }
    UIImageView *imgCamView1=[[UIImageView alloc] initWithImage:self.image1];
    imgCamView1.frame=CGRectMake(0, -20, 320, 400);
    [scrollCamView addSubview:imgCamView1];
    scrollCamView.contentSize=CGSizeMake(320, 300);
    
    
    
    /*UIImageView *imgView1=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_1.png"]];
    imgView1.frame=CGRectMake(0, 0, 320, 190);
    [scrollView addSubview:imgView1];
    UIImageView *imgView2=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stage_3.png"]];
    imgView2.frame=CGRectMake(320, 0, 320, 190);
    [scrollView addSubview:imgView2];*/
    [self imageStage];
    //scrollView.contentSize=CGSizeMake(320*12, 190);
    //[self.view addSubview:tnavController.view];

    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[self dismissModalViewControllerAnimated:YES];
}


-(IBAction)CamPicStageConfirmTapped:(id)button{
    //NSLog(@"confirm");
    /*UploadGalleryController *GUploadGalleryController=[[UploadGalleryController alloc] initWithNibName:@"UploadGalleryController"  bundle:nil];
    
    GUploadGalleryController.image1= self.image1;
    NSString *appLabel=self.pageNumberLabel.text;
    
    GUploadGalleryController.pageNumberLabel.text=appLabel;*/
    
    [self switchToUploadGalleryController];
} 

-(void)switchToUploadGalleryController
{
    UploadGalleryController *GUploadGalleryController=[[UploadGalleryController alloc] initWithNibName:@"UploadGalleryController"  bundle:nil];
    GUploadGalleryController.image1= self.image1;
    
    
    
    UIImageView *imgCamViewBlank=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"main_page.png"]];
    imgCamViewBlank.frame=CGRectMake(0, -20, 320, 400);
    GUploadGalleryController.image1=self.image1;
    
    GUploadGalleryController._text=self.pageNumberLabel.text;
  
    //GUploadGalleryController.StageDesc=self.StageDesc;
    testClasssAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.StageDesc=self.StageDesc;
    
    /*UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:GUploadGalleryController];
    
    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:tnavController.view];*/

	
    //[self.view addSubview:GUploadGalleryController.view];
     
    //[self.navigationController pushViewController:GUploadGalleryController animated:YES];
    [self presentModalViewController:GUploadGalleryController animated:YES];
}



@end



