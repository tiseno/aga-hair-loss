//
//  CollectPatientDetail.m
//  test
//
//  Created by Tiseno Mac 2 on 12/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CollectPatientDetail.h"
#import <QuartzCore/QuartzCore.h>

@implementation CollectPatientDetail
@synthesize PatientDetailView;
@synthesize PatientNameText;
@synthesize RemarkText;
@synthesize StageText;
@synthesize PatientImageView;
@synthesize image1,_text,_textPatientName,_textRemark, StageDesc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.PatientImageView.image=image1;
    self.PatientNameText.text=_textPatientName;
    self.RemarkText.text=_textRemark;
    self.StageText.text=_text;
    testClasssAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    self.txtStageDesc.text=appDelegate.StageDesc;

    
    NSLog(@"StageDesc>>>%@",self.txtStageDesc.text);
    
    /*UIGraphicsBeginImageContext(PatientDetailView.frame.size);
    [self.PatientDetailView.layer drawInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage_after = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(viewImage_after, nil, nil, nil);*/
    [self renderToImage];
}

- (UIImage*) renderToImage
{
    // IMPORTANT: using weak link on UIKit
    if(UIGraphicsBeginImageContextWithOptions != NULL)
    {
        UIGraphicsBeginImageContextWithOptions(PatientDetailView.frame.size, NO, 0.0);
    } else {
        UIGraphicsBeginImageContext(PatientDetailView.frame.size);
    }
    
    [PatientDetailView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();  
    
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    return nil;
}

- (void)viewDidUnload
{
    [self setPatientNameText:nil];
    [self setRemarkText:nil];
    [self setStageText:nil];
    [self setPatientImageView:nil];
    [self setPatientDetailView:nil];
    [self setTxtStageDesc:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)AddNewCase:(id)sender;
{
    StartPageController *GStartPageController=[[StartPageController alloc] initWithNibName:@"StartPageController"  bundle:nil];
    
    for (UIView* uiview in self.view.subviews) {
        [uiview removeFromSuperview];
    }
    [self.view addSubview:GStartPageController.view];
}

-(IBAction)ExitButton:(id)sender;
{
    ExitPageController *GExitPageController=[[ExitPageController alloc] initWithNibName:@"ExitPageController"  bundle:nil];
    
    for (UIView* uiview in self.view.subviews) {
        [uiview removeFromSuperview];
    }
    [self.view addSubview:GExitPageController.view];
}

- (void)dealloc
{
    [StageDesc release];
    [PatientNameText release];
    [RemarkText release];
    [StageText release];
    [PatientImageView release];
    [PatientDetailView release];
    [_txtStageDesc release];
    [super dealloc];
}
@end
