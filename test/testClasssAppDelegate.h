//
//  testClasssAppDelegate.h
//  test
//
//  Created by Tiseno Mac 2 on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class testClasssViewController;
@class StartPageController;

@interface testClasssAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//@property (strong, nonatomic) testClasssViewController *viewController;
@property (strong, nonatomic) StartPageController *viewController;
@property (nonatomic, retain) NSString *StageDesc;
@end
