//
//  TextInsertNameController.m
//  test
//
//  Created by Tiseno Mac 2 on 12/9/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "TextInsertNameController.h"

@implementation TextInsertNameController
@synthesize leftBarButtonItem;
@synthesize PatientNameTextField;
@synthesize PatientName;
@synthesize TmpPatientName;
@synthesize image1, _gtext, _gtextRemark;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
//-(void)viewDidAppear:(BOOL)animated
//{
    //[PatientNameTextField becomeFirstResponder];
    //[super viewWillAppear:animated];
    //[super viewDidAppear:animated];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];

//}

/*-(void)viewWillAppear:(BOOL)animated 
{
	    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}*/


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    /*UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    self.title = NSLocalizedString(@"Patient", @"");     
	//[self setupTextView];
    
    //if([PatientNameTextField isFirstResponder])[PatientNameTextField becomeFirstResponder];
    PatientNameTextField.editable = NO;
    PatientNameTextField.editable = YES;
    [PatientNameTextField becomeFirstResponder];
    [self.view addSubview:PatientNameTextField];
    
    PatientNameTextField.delegate = self;
    [self.navigationItem setTitle:@"Patient"];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    //[self viewDidDisappear:YES];*/
    
    
    
    
    
    //[PatientNameTextField resignFirstResponder];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        
    //[self.view addSubview:PatientNameTextField];
    //[PatientNameTextField becomeFirstResponder]; 
     
    
    //PatientNameTextField.editable=NO;
    //PatientNameTextField.editable=YES;
        
    [[PatientNameTextField layer] setBorderWidth:2.3];
	[[PatientNameTextField layer] setCornerRadius:15];
    //[PatientNameTextField becomeFirstResponder];
    //[PatientNameTextField becomeFirstResponder];
}




- (void)viewDidUnload
{
    [self setPatientNameTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)BtnConfirmPatientName:(id)theTextField
{
    
    self.PatientName=self.PatientNameTextField.text;
    //NSString *nameString = self.PatientName;
    //if ([nameString length]==0){
        //nameString=@"World";
        
    //}
    //NSString *greeting =[[NSString alloc]initWithFormat:@"Hello, %@!",nameString];
    //self.lable.text=greeting;
    
    
    UploadGalleryController *GUploadGalleryController=[[UploadGalleryController alloc] initWithNibName:@"UploadGalleryController"  bundle:nil];
    GUploadGalleryController.image1= self.image1;
    
    
    GUploadGalleryController._text=self._gtext;
    GUploadGalleryController._textRemark=self._gtextRemark;
    
    self.PatientName=self.PatientNameTextField.text;
    
    GUploadGalleryController._textPatientName=self.PatientName;
    
    
    /*UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:GUploadGalleryController];
    
    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:tnavController.view];*/
    
	
    //[self.view addSubview:GUploadGalleryController.view];
    //[self.PatientNameTextField resignFirstResponder];
    //[self.navigationController pushViewController:GUploadGalleryController animated:YES];
    [self presentModalViewController:GUploadGalleryController animated:YES];

}

-(IBAction)BtnCancelDetail:(id)buttoncancel
{
    self.PatientName=self.PatientNameTextField.text;
    UploadGalleryController *GUploadGalleryController=[[UploadGalleryController alloc] initWithNibName:@"UploadGalleryController"  bundle:nil];
    GUploadGalleryController.image1= self.image1;
    
    
    GUploadGalleryController._text=self._gtext;
    GUploadGalleryController._textRemark=self._gtextRemark;
    
    //self.PatientName=self.PatientNameTextField.text;
    
    GUploadGalleryController._textPatientName=self.TmpPatientName;
    
    
    /*UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:GUploadGalleryController];
    
    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:tnavController.view];*/
    
	
    [self.view addSubview:GUploadGalleryController.view];
    [self.PatientNameTextField resignFirstResponder];

}

/*-(void)setupTextView
{
	self.PatientNameTextField = [[[UITextView alloc] initWithFrame:self.view.frame] autorelease];
    //self.PatientNameTextField = [[UITextView alloc]  autorelease];
    
	self.PatientNameTextField.textColor = [UIColor blackColor];
	self.PatientNameTextField.font = [UIFont fontWithName:@"Arial" size:18];
	self.PatientNameTextField.delegate = self;
	self.PatientNameTextField.backgroundColor = [UIColor whiteColor];
	
	self.PatientNameTextField.text = @"";
	self.PatientNameTextField.returnKeyType = UIReturnKeyDefault;
	self.PatientNameTextField.keyboardType = UIKeyboardTypeDefault;	// use the default type input method (entire keyboard)
	self.PatientNameTextField.scrollEnabled = YES;
	
	// this will cause automatic vertical resize when the table is resized
	self.PatientNameTextField.autoresizingMask = UIViewAutoresizingFlexibleHeight;
	// note: for UITextView, if you don't like autocompletion while typing use:
	// myTextView.autocorrectionType = UITextAutocorrectionTypeNo;
	[self.view addSubview: self.PatientNameTextField];  
}*/

/*- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text 
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)viewDidDisappear:(BOOL)animated 
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}*/

/*-(void)keyboardWillShow:(NSNotification *)aNotification 
{
	CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.size.height -= keyboardRect.size.height;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

-(void)keyboardWillHide:(NSNotification *)aNotification
{
   	CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.size.height += keyboardRect.size.height;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}*/

/*- (void)textViewDidBeginEditing:(UITextView *)textView
{
	// provide my own Save button to dismiss the keyboard
	UIBarButtonItem* saveItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
																			  target:self action:@selector(BtnConfirmPatientName:)];
	self.navigationItem.rightBarButtonItem = saveItem;
	[saveItem release];
}


- (void)saveAction:(id)sender
{
	// finish typing text/dismiss the keyboard by removing it as the first responder
	[self.PatientNameTextField resignFirstResponder];
	self.navigationItem.rightBarButtonItem = nil;	// this will remove the "save" button
}*/


-(void)dealloc
{
    [PatientNameTextField release];
    [leftBarButtonItem release];
    [PatientName release];
    [image1 release];
    [_gtext release];
    [_gtextRemark release];
    [TmpPatientName release];
}

@end
