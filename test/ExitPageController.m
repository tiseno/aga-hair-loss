//
//  ExitPageController.m
//  test
//
//  Created by Tiseno Mac 2 on 12/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ExitPageController.h"


@implementation ExitPageController
//@synthesize secondsA;
@synthesize _CountDownTime;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)updateTimer{ //Happens every time updateTimer is called. Should occur every second.
    
    counterA -= 1;
    _CountDownTime = [NSString stringWithFormat:@"%i", counterA];
    
    if (counterA < 0) // Once timer goes below 0, reset all variables.
    {
        
        //secondsA.text = @"Done";
        
        testClasssViewController *GtestClasssViewController=[[testClasssViewController alloc] initWithNibName:@"testClasssViewController"  bundle:nil];
        
        for (UIView* uiview in self.view.subviews) {
            [uiview removeFromSuperview];
        }
        [self.view addSubview:GtestClasssViewController.view];
        
        [timerA invalidate];
        startA = TRUE;
        counterA = 1;
        
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad
 {
 [super viewDidLoad];
     counterA = 1;
     startA = TRUE;
     
     if(startA == TRUE) //Check that another instance is not already running.
     {
         _CountDownTime = @"1";
         startA = FALSE;
         timerA = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
     }

 }



- (void)viewDidUnload
{
    //[self setSecondsA:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [secondsA release];
    [super dealloc];
}
@end
