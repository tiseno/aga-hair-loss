//
//  TextInsertRemark.h
//  test
//
//  Created by Tiseno Mac 2 on 12/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadGalleryController.h"
#import <QuartzCore/QuartzCore.h>

@interface TextInsertRemark : UIViewController< UITextFieldDelegate, UITextViewDelegate>{
    //UIBarButtonItem *leftBarButtonItem;
    UITextView *RemarkTextField;
    NSString *StringRemark;
    UIBarButtonItem* dismissKeyboardButton;
    UIImage *image1;
    NSString *_gtext;
    NSString *_textPatientName;
    NSString *_TmpStringRemark;
    }


@property (nonatomic,retain) NSString *_gtext;
@property (nonatomic,retain) NSString *_textPatientName;
@property (nonatomic,retain) UIImage *image1;
//@property(nonatomic, retain) UIBarButtonItem *leftBarButtonItem;
@property (retain, nonatomic) IBOutlet UITextView *RemarkTextField;

@property (nonatomic, copy) NSString *StringRemark;
@property (nonatomic, copy) NSString *_TmpStringRemark;
-(IBAction)BtnConfirmRemark:(id)sender;
-(IBAction)BtnCancelRemark:(id)sender;


@end
