//
//  UploadGalleryController.m
//  test
//
//  Created by Tiseno Mac 2 on 12/8/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UploadGalleryController.h"

@implementation UploadGalleryController
@synthesize testingView;
//@synthesize maskedImageView;
@synthesize ImageBackround;
@synthesize CamImageView, scrollCamView;
@synthesize pageNumberLabel;
@synthesize image1,uploadedImage1Path;
@synthesize _text,_textPatientName, _textRemark;
@synthesize textView;
@synthesize TextFieldRemark;
@synthesize TextFieldPatientName;
@synthesize StageDesc;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
     self.navigationItem.backBarButtonItem = backButton;
     [[self navigationItem] setBackBarButtonItem:backButton];*/
    
 
    
    pageNumberLabel.text=_text;
    TextFieldPatientName.text=_textPatientName;
    TextFieldRemark.text=_textRemark;
    
    
    UIImageView *imgCamView1=[[UIImageView alloc] initWithImage:self.image1];
    imgCamView1.frame=CGRectMake(0, -20, 320, 400);
    
    //self.CamImageView.image=self.image1;
    
    [scrollCamView addSubview:imgCamView1];
    scrollCamView.contentSize=CGSizeMake(320, 300);
    //dismissKeyboardButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissKeyboard)];
    
    //[[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    
}

- (void)viewDidUnload
{
    [self setCamImageView:nil];
    //[self setPageNumberLabel:nil];
    self.textView = nil;
    [self setTextFieldPatientName:nil];
    [self setTextFieldRemark:nil];
    //[self setMaskedImageView:nil];
    [self setImageBackround:nil];
    [self setTestingView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)editingChanged:(id)sender
{
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //NSLog(@"textbox begin");
    //self.navigationItem.rightBarButtonItem = dismissKeyboardButton;
        
    //keyboardToolbarShouldHide = YES;
    
    
    if (textField == TextFieldPatientName) {
        //NSLog(@"textbox begin1");
        
		//[TextFieldPatientName becomeFirstResponder];
        //TextFieldPatientName.enabled=YES;
        
        [TextFieldPatientName resignFirstResponder];
        
        TextInsertNameController *GTextInsertNameController=[[TextInsertNameController alloc] initWithNibName:@"TextInsertNameController"  bundle:nil];
    GTextInsertNameController.image1= self.image1;
        GTextInsertNameController._gtextRemark=self._textRemark;
        GTextInsertNameController.TmpPatientName=self._textPatientName;
        GTextInsertNameController.PatientNameTextField.delegate=self;
    GTextInsertNameController._gtext=self.pageNumberLabel.text;
        
        
        //[self.navigationController pushViewController:GTextInsertNameController animated:YES];
        
        [self presentModalViewController:GTextInsertNameController animated:YES];
        
        /*UITextField *nameTextField = [[UITextField alloc] initWithFrame: CGRectMake(100, 260, 320, 260)];
        
        nameTextField.transform = CGAffineTransformMakeRotation(M_PI * (90.0 / 180.0)); // rotate for landscape
        
        // NOTE: UITextField won't be visible by default without setting backGroundColor & borderStyle
        nameTextField.backgroundColor = [UIColor clearColor];
        nameTextField.borderStyle = UITextBorderStyleRoundedRect;
        
        nameTextField.delegate = self; // set this layer as the UITextFieldDelegate
        nameTextField.returnKeyType = UIReturnKeyDone; // add the 'done' key to the keyboard
        nameTextField.autocorrectionType = UITextAutocorrectionTypeNo; // switch of auto correction
        nameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        
        UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:GTextInsertNameController];

        tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        for(UIView *tview in self.view.subviews)
        {
            [tview removeFromSuperview];
        }
        [self.view addSubview:tnavController.view];
        // add the textField to the main game openGLVview
        // auto show the keyboard once the view has been added (NB: seems to need to be AFTER addSubview call to work)
        [nameTextField becomeFirstResponder];*/
        
        
	} else if (textField == TextFieldRemark) {
		//[TextFieldRemark becomeFirstResponder];
        //TextFieldRemark.enabled = YES;
        
        //[TextFieldRemark resignFirstResponder];
        TextInsertRemark *GTextInsertRemark=[[TextInsertRemark alloc] initWithNibName:@"TextInsertRemark"  bundle:nil];
        GTextInsertRemark.image1= self.image1;
        GTextInsertRemark._textPatientName=self._textPatientName;
        GTextInsertRemark._TmpStringRemark=self._textRemark;
        GTextInsertRemark.RemarkTextField.delegate=self;
        GTextInsertRemark._gtext=self.pageNumberLabel.text;
        
        //[self.navigationController pushViewController:GTextInsertRemark animated:YES];
        [self presentModalViewController:GTextInsertRemark animated:YES];
	} 		
            
}

/*- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
		return NO;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    //TextFieldPatientName.text=_textPatientName;
}*/




-(IBAction)BtnBackDetail:(id)button
{
    ScrollViewController *GScrollViewController=[[ScrollViewController alloc] initWithNibName:@"ScrollViewController"  bundle:nil];
    GScrollViewController.image1= self.image1;
    
    //GScrollViewController._textPatientName=self._textPatientName;
    //GScrollViewController._textRemark=self._textRemark;
    
    GScrollViewController.pageNumberLabel.text=self._text;
    
    
    //[self.view addSubview:GScrollViewController.view];
    //[self.navigationController pushViewController:GSaveDetailPictureController animated:YES];
    [self presentModalViewController:GScrollViewController animated:YES];
}

-(void)transerToConfirm
{
    for (UIView* uiview in self.view.subviews) {
        [uiview removeFromSuperview];
    }
    /**/CollectPatientDetail *GCollectPatientDetail=[[CollectPatientDetail alloc] initWithNibName:@"CollectPatientDetail"  bundle:nil];
    GCollectPatientDetail.image1= self.image1;
    
    GCollectPatientDetail._textPatientName=self._textPatientName;
    GCollectPatientDetail._textRemark=self._textRemark;
    GCollectPatientDetail._text=self.pageNumberLabel.text;
    //GCollectPatientDetail.StageDesc=self.StageDesc;
    
    
    //[self.view addSubview:GSaveDetailPictureController.view];
    //[self.navigationController pushViewController:GSaveDetailPictureController animated:YES];
    //[self presentModalViewController:GSaveDetailPictureController animated:YES];
    /**/
    [self.view addSubview:GCollectPatientDetail.view];
    
    

}

-(IBAction)BtnConfirmDetail:(id)button
{
     
        //[self saveInAFile];
    
    [self transerToConfirm];
}


- (void)dealloc
{
    [StageDesc release];
    [CamImageView release];
    [scrollCamView release];
    [image1 release];
    [textView release];
    [_text release];
    [_textPatientName release];
    [_textRemark release];
    [pageNumberLabel release];
    [TextFieldPatientName release];
    [TextFieldRemark release];
    //[maskedImageView release];
    [ImageBackround release];
    [testingView release];
    [super dealloc];
}
@end
