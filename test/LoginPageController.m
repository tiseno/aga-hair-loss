//
//  LoginPageController.m
//  test
//
//  Created by Tiseno Mac 2 on 12/30/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "LoginPageController.h"

@implementation LoginPageController
@synthesize UserNameText;
@synthesize PasswordText;
@synthesize _gUserNametext,_gPasswordtext;
//@synthesize _username1;
//@synthesize _password1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UserNameText.text= _gUserNametext;
    PasswordText.text= _gPasswordtext;
    //self._username1= @"admin";
    //self._password1= @"12345";
}

- (void)viewDidUnload
{
    [self setUserNameText:nil];
    [self setPasswordText:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)BtnLogin:(id)sender
{
    //static NSString *_username1 = @"admin";
    //static NSString *_password1 = @"12345";
    
    if (_gUserNametext != nil & _gPasswordtext != nil) {


        if ([UserNameText.text isEqualToString:@"adminaga"] & [PasswordText.text isEqualToString:@"admin123"]) {
     
     StartPageController *GStartPageController=[[StartPageController alloc] initWithNibName:@"StartPageController"  bundle:nil];
        
        [self.view addSubview:GStartPageController.view];
    
            NSLog(@"OK1!");
            
        }else if ([UserNameText.text isEqualToString:@"memberaga"] & [PasswordText.text isEqualToString:@"aga123@"]){
            StartPageController *GStartPageController=[[StartPageController alloc] initWithNibName:@"StartPageController"  bundle:nil];
             
             [self.view addSubview:GStartPageController.view];
            NSLog(@"OK2!");
            
        }else if ([UserNameText.text isEqualToString:@"agalogin"] & [PasswordText.text isEqualToString:@"86893858"]){
            StartPageController *GStartPageController=[[StartPageController alloc] initWithNibName:@"StartPageController"  bundle:nil];
             
             [self.view addSubview:GStartPageController.view];
            NSLog(@"OK3!");
            
        }else if ([UserNameText.text isEqualToString:@"hairlossaga"] & [PasswordText.text isEqualToString:@"hairloss123"]){
            StartPageController *GStartPageController=[[StartPageController alloc] initWithNibName:@"StartPageController"  bundle:nil];
             
             [self.view addSubview:GStartPageController.view];
            NSLog(@"OK4!");
            
        }else if ([UserNameText.text isEqualToString:@"aga2012"] & [PasswordText.text isEqualToString:@"aga2012@"]){
            StartPageController *GStartPageController=[[StartPageController alloc] initWithNibName:@"StartPageController"  bundle:nil];
             
             [self.view addSubview:GStartPageController.view];
            NSLog(@"OK5!");
            
        }else if ([UserNameText.text isEqualToString:@"agaapple"] & [PasswordText.text isEqualToString:@"icube123@"]){
            StartPageController *GStartPageController=[[StartPageController alloc] initWithNibName:@"StartPageController"  bundle:nil];
             
             [self.view addSubview:GStartPageController.view];
            NSLog(@"OK6!");
            
        }else if ([UserNameText.text isEqualToString:@"luckystar888"] & [PasswordText.text isEqualToString:@"88888888"]){
            StartPageController *GStartPageController=[[StartPageController alloc] initWithNibName:@"StartPageController"  bundle:nil];
             
             [self.view addSubview:GStartPageController.view];
            NSLog(@"OK7!");
            
        }else if ([UserNameText.text isEqualToString:@"goodday7"] & [PasswordText.text isEqualToString:@"1234567@"]){
            StartPageController *GStartPageController=[[StartPageController alloc] initWithNibName:@"StartPageController"  bundle:nil];
             
             [self.view addSubview:GStartPageController.view];
            NSLog(@"OK8!");
            
        }else if ([UserNameText.text isEqualToString:@"useraga"] & [PasswordText.text isEqualToString:@"useraga2012"]){
            StartPageController *GStartPageController=[[StartPageController alloc] initWithNibName:@"StartPageController"  bundle:nil];
             
             [self.view addSubview:GStartPageController.view];
            NSLog(@"OK9!");
            
        }else if ([UserNameText.text isEqualToString:@"testing"] & [PasswordText.text isEqualToString:@"12345"]){
            StartPageController *GStartPageController=[[StartPageController alloc] initWithNibName:@"StartPageController"  bundle:nil];
             
             [self.view addSubview:GStartPageController.view];
            NSLog(@"OK10!");
            
        }else {
        NSLog(@"wrong UserName/password!");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"The username and password entered are not valid"
                                                            message:nil
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert autorelease];
            [alert show];

        }
        
        //NSLog(@"OK!");

    
        
    } else {
        
        NSLog(@"empty text box!");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please insert username and password"
                                                        message:nil
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert autorelease];
        [alert show];

    }
    
    
    
    

}

-(IBAction)BtnCancelLogin:(id)sender
{
    testClasssViewController *GtestClasssViewController=[[testClasssViewController alloc] initWithNibName:@"testClasssViewController"  bundle:nil];
   	
    [self.view addSubview:GtestClasssViewController.view];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //NSLog(@"textbox begin");
    //self.navigationItem.rightBarButtonItem = dismissKeyboardButton;
    
    //keyboardToolbarShouldHide = YES;
    
    
    if (textField == UserNameText) {
        //NSLog(@"textbox begin1");
        
		[UserNameText becomeFirstResponder];
        //TextFieldPatientName.enabled=YES;
        
        [UserNameText resignFirstResponder];
        
        UserNameInsertController *GUserNameInsertController=[[UserNameInsertController alloc] initWithNibName:@"UserNameInsertController"  bundle:nil];
        GUserNameInsertController._gUserNametext= self._gUserNametext;
        GUserNameInsertController._gPasswordtext= self._gPasswordtext;
                        
        
        //[self.navigationController pushViewController:GTextInsertNameController animated:YES];
        
        [self presentModalViewController:GUserNameInsertController animated:YES];
        
      
        
        
	} else if (textField == PasswordText) {
		[PasswordText becomeFirstResponder];
        //TextFieldRemark.enabled = YES;
        
        [PasswordText resignFirstResponder];
        PasswordInsertController *GPasswordInsertController=[[PasswordInsertController alloc] initWithNibName:@"PasswordInsertController"  bundle:nil];
        GPasswordInsertController._gUserNametext= self._gUserNametext;
        GPasswordInsertController._gPasswordtext= self._gPasswordtext;
        
        //[self.navigationController pushViewController:GTextInsertRemark animated:YES];
        [self presentModalViewController:GPasswordInsertController animated:YES];
	} 		
    
}


- (void)dealloc {
    [UserNameText release];
    [PasswordText release];
    [_gUserNametext release];
    [_gPasswordtext release];
    
    //[_username1 release];
    //[_password1 release];
        
    [super dealloc];
}
@end
