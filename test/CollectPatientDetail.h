//
//  CollectPatientDetail.h
//  test
//
//  Created by Tiseno Mac 2 on 12/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StartPageController.h"
#import "ExitPageController.h"
#import "testClasssAppDelegate.h"

@interface CollectPatientDetail : UIViewController{
    UILabel *PatientNameText;
    UILabel *RemarkText; 
    UILabel *StageText;
    UIImageView *PatientImageView;
    UIImage *image1;
    NSString *_text;
    NSString *_textPatientName;
    NSString *_textRemark;
    UIView *PatientDetailView;
}


@property (retain, nonatomic) IBOutlet UILabel *PatientNameText;
@property (retain, nonatomic) IBOutlet UILabel *RemarkText;
@property (retain, nonatomic) IBOutlet UILabel *StageText;
@property (retain, nonatomic) IBOutlet UIImageView *PatientImageView;
@property (nonatomic, retain) NSString *StageDesc;
@property (retain, nonatomic) IBOutlet UILabel *txtStageDesc;

@property (nonatomic,retain) UIImage *image1;
@property (nonatomic,retain) NSString *_text;
@property (nonatomic,retain) NSString *_textPatientName;
@property (nonatomic,retain) NSString *_textRemark;

@property (retain, nonatomic) IBOutlet UIView *PatientDetailView;


- (UIImage*) renderToImage;
-(IBAction)AddNewCase:(id)sender;
-(IBAction)ExitButton:(id)sender;


@end
