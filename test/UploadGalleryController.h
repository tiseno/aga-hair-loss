//
//  UploadGalleryController.h
//  test
//
//  Created by Tiseno Mac 2 on 12/8/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "TextInsertNameController.h"
#import "TextInsertRemark.h"
#import "CollectPatientDetail.h"
#import "ScrollViewController.h"
#import "testClasssAppDelegate.h"

@interface UploadGalleryController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate,UITextFieldDelegate>{
    UIImage *image1;
    //UIImage *image2;
    UIImageView *CamImageView;
    UIScrollView *scrollCamView;
    UILabel *pageNumberLabel;
    NSString *_text;
    //UIBarButtonItem* dismissKeyboardButton;
    UITextView *textView;
    UITextField *TextFieldPatientName;
    NSString *_textPatientName;
    //NSString *_TPatientName;
    UITextField *TextFieldRemark;
    NSString *_textRemark;
    //UIImageView *maskedImageView;
    UIImageView *ImageBackround;
    NSString *uploadedImage1Path;
    UIView *testingView;
}

@property (nonatomic, retain) NSString *StageDesc;
- (IBAction)editingChanged:(id)sender;
@property (retain, nonatomic) IBOutlet UIView *testingView;


//-(UIImage *)addText:(UIImage *)img text:(NSString *)text1;
//- (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage;
@property (nonatomic, retain) UITextView *textView;
@property (retain, nonatomic) IBOutlet UITextField *TextFieldRemark;

@property (retain, nonatomic) IBOutlet UITextField *TextFieldPatientName;
@property (nonatomic,retain) NSString *_text;
@property (nonatomic,retain) NSString *_textPatientName;
@property (nonatomic,retain) NSString *_textRemark;
//@property (nonatomic,retain) NSString *_TPatientName;

@property (retain, nonatomic) IBOutlet UIImageView *CamImageView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollCamView;

@property (retain, nonatomic) IBOutlet UILabel *pageNumberLabel;
//@property (nonatomic,retain) IBOutlet NSString *pageNumberLabel;
@property (nonatomic,retain) UIImage *image1;
//@property (nonatomic,retain) UIImage *image2;
@property (nonatomic, retain) NSString *uploadedImage1Path;

-(IBAction)BtnConfirmDetail:(id)button;

//@property (retain, nonatomic) IBOutlet UIImageView *maskedImageView;
@property (retain, nonatomic) IBOutlet UIImageView *ImageBackround;

-(IBAction)BtnBackDetail:(id)button;

@end
