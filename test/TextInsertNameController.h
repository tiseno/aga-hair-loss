//
//  TextInsertNameController.h
//  test
//
//  Created by Tiseno Mac 2 on 12/9/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadGalleryController.h"
#import <QuartzCore/QuartzCore.h>
//NSString *const UIKeyboardDidShowNotification;

@interface TextInsertNameController : UIViewController<UITextFieldDelegate, UITextViewDelegate>{
    //UIBarButtonItem *leftBarButtonItem;
    UITextView *PatientNameTextField;
    NSString *PatientName;
    NSString *TmpPatientName;
    //UIBarButtonItem* dismissKeyboardButton;
    UIImage *image1;
    NSString *_gtext;
    NSString *_gtextRemark;
    
}

@property (nonatomic,retain) NSString *_gtext;
@property (nonatomic,retain) NSString *_gtextRemark;
@property (nonatomic,retain) UIImage *image1;
@property(nonatomic, retain) UIBarButtonItem *leftBarButtonItem;
@property (retain, nonatomic) IBOutlet UITextView *PatientNameTextField;

//@property(nonatomic, retain) UITextField *PatientNameTextField;
@property (nonatomic, copy) NSString *PatientName;
@property (nonatomic, copy) NSString *TmpPatientName;
-(IBAction)BtnConfirmPatientName:(id)sender;
-(IBAction)BtnCancelDetail:(id)buttoncancel;

@end
